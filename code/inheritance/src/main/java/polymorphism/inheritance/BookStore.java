package polymorphism.inheritance;

public class BookStore {
    public static void main(String[] args){
        Book[] quintuple = new Book[5];
        quintuple[0] = new Book("First Blood", "John Rambo");
        quintuple[1] = new ElectronicBook("Rambo: First Blood Part 2", "James Rambo", 2000);
        quintuple[2] = new Book("Rambo III", "Juiced Rambo");
        quintuple[3] = new ElectronicBook("Rambo", "JHGH Rambo", 6000);
        quintuple[4] = new ElectronicBook("Rambo: Last Blood", "JumpTheShark Rambo", 9000);

        for (int i = 0; i < quintuple.length; i++){
            System.out.println(quintuple[i]);
        }
    }
}

/*
 * PART 4 QUESTIONS
 * 
 * 1) Book's
 * 
 * 2) ElectronicBook's
 * 
 * 3) Compiler error. Says that method is undefined for type Book.
 * 
 * 4) This will work... was my guess. But it actually doesn't; same error. quintuple was declared as an array of Book. So it will still look at each index as a book, even though ElectroincBook "is a" Book.
 * 
 * 5) Still won't work. Like in 4, all in the array are consider "Book" items. Type mismatch.
 * 
 * 6) This should work.
 * 
 * 7) Thinking it might work. It does.
 * 
 * 8) It will work. Not sure what it will print, but maybe 0 since no value was assigned. So it compiles, but get RTE --> ClassCastException.
 *      Basically, ElectronicBook "is a" Book (quintuple[1], an ElectronicBook, is a Book and also of course an ElectronicBook). But a Book isn't an ElectronicBook.
 */
