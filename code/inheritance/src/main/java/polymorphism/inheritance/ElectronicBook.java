package polymorphism.inheritance;

public class ElectronicBook extends Book {
    private int numberBytes;

    public ElectronicBook(String newTitle, String newAuthor, int newNumberBytes){
        super(newTitle, newAuthor);

        // this.author = newAuthor; if you try this, you get compiler error that this field is not visible

        this.numberBytes = newNumberBytes;
    }
    
    public int getNumberBytes(){
        return this.numberBytes;
    }

    public String toString(){
        // Do either this

        return "Title: " + this.getTitle() + "\nAuthor: " + this.getAuthor() + "\nNumber of Bytes: " + this.numberBytes;

        /*
         * OR:
         * 
         * return Super.toString() + "\nNumber of Bytes: " + this.numberBytes;
         * 
         * The Super.toString() returns the toString() output of what this object inherits from
         * 
         */
    }
}
